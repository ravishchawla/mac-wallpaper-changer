from AppKit import NSWorkspace, NSScreen, NSHomeDirectory, NSUserNotification, NSUserNotificationCenter
from Foundation import NSURL;
import os;
import random;
import pickle;

def notifiy_on_screen(directory, picture_item):
	notification = NSUserNotification.alloc().init();
	notification.setTitle_("Setting " + picture_item);
	notification.setSubtitle_("from " + directory);
	NSUserNotificationCenter.defaultUserNotificationCenter().scheduleNotification_(notification);

def setImageWallpaper(pictures_path):
	print(pictures_path);

def getRandomImages(pictures_folder, total_pictures, allowed_exts):
	files_list = [];

	for root, subFolders, files in os.walk(pictures_folder):
		for file in files:
			if not root.endswith("~") and "~/" not in root and file.endswith(allowed_exts):
				files_list.append(root + "/" + file);

	return random.sample(files_list, total_pictures);



def getRandomImage(pictures_folder):
	random_item = "";
	last_item = "";
	while True:
		dirs = os.listdir(pictures_folder);
		dir_contents = [];
		for dir in dirs:
			if not dir.endswith("~"):
				dir_contents.append(dir);

		last_item = random_item;
		random_item = random.choice(dir_contents);
		pictures_folder = pictures_folder + "/" + random_item;

		if not os.path.isdir(pictures_folder):
			break;

	return random_item, last_item, pictures_folder;

def loadPicturesList():
	try:
		pictures_list = pickle.load(open('my_wallpapers_test .p', "rb"));
	except IOError:
		pictures_list = [];

	return pictures_list;

def savePicturesList(pictures_list):
	try:
		pickle.dump(pictures_list, open('my_wallpapers_test.p', 'wb'));
	except IOError:
		return False;

def initVars():
	pictures_list = [];

	return pictures_list, main_dir;

if __name__ == "__main__":

	pictures_list = loadPicturesList();

	if len(pictures_list) == 0:
		home = NSHomeDirectory();
		main_dir = home + "/" + "Pictures/MegaSync";
		allowed_exts = ("png", "jpg", "jpeg");
		pictures_list = getRandomImages(main_dir, 100, allowed_exts);

	for p in pictures_list:
		print(p + "\n");

	next_picture = pictures_list.pop();

	picture_name = os.path.split(next_picture)[1];
	directory = os.path.split(os.path.dirname(next_picture))[1];

	setImageWallpaper(next_picture);
	notifiy_on_screen(directory, picture_name);


	#savePicturesList(pictures_list);


