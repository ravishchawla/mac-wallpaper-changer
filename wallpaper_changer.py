from AppKit import NSWorkspace, NSScreen, NSHomeDirectory, NSUserNotification, NSUserNotificationCenter
from Foundation import NSURL;
import os;
import random;
import pickle;
from PIL import Image

def notifiy_on_screen(directory, picture_item):
	notification = NSUserNotification.alloc().init();
	notification.setTitle_(picture_item);
	notification.setSubtitle_(directory);
	NSUserNotificationCenter.defaultUserNotificationCenter().scheduleNotification_(notification);

def setImageWallpaper(pictures_path):
	file_url = NSURL.fileURLWithPath_(pictures_path);

	options = {}
	 
	# get shared workspace
	ws = NSWorkspace.sharedWorkspace();

	screen = NSScreen.mainScreen();
    # tell the workspace to set the desktop picture
	(result, error) = ws.setDesktopImageURL_forScreen_options_error_(file_url, screen, options, None);
	if error:
		print(error)
		exit(-1)
	'''
	 
	# iterate over all screens
	for screen in NSScreen.screens():
	    # tell the workspace to set the desktop picture
	    (result, error) = ws.setDesktopImageURL_forScreen_options_error_(
	                file_url, screen, options, None)
	    if error:
	        print(error)
	        exit(-1)

	'''

def getRandomImages(pictures_folder, total_pictures, allowed_exts):
	files_list = [];

	for root, subFolders, files in os.walk(pictures_folder):
		for file in files:
			if not root.endswith("~") and "~/" not in root and file.endswith(allowed_exts):
				files_list.append(root + "/" + file);

	return random.sample(files_list, total_pictures);



def getRandomImage(pictures_folder):
	random_item = "";
	last_item = "";
	while True:
		dirs = os.listdir(pictures_folder);
		dir_contents = [];
		for dir in dirs:
			if not dir.endswith("~"):
				dir_contents.append(dir);

		last_item = random_item;
		random_item = random.choice(dir_contents);
		pictures_folder = pictures_folder + "/" + random_item;

		if not os.path.isdir(pictures_folder):
			break;

	return random_item, last_item, pictures_folder;

def loadPicturesList():
	try:
		pictures_list = pickle.load(open("/Users/ravishchawla/workspace/mac-wallpaper-changer/" + 'my_wallpapers.p', "rb"));
	except IOError:
		pictures_list = [];

	return pictures_list;

def savePicturesList(pictures_list):
	try:
		pickle.dump(pictures_list, open("/Users/ravishchawla/workspace/mac-wallpaper-changer/" + 'my_wallpapers.p', 'wb'));
	except IOError as err:
		print(err)
		return False;

def initVars():
	pictures_list = [];	

	return pictures_list, main_dir;

if __name__ == "__main__":

	pictures_list = loadPicturesList();

	if len(pictures_list) == 0:
		home = NSHomeDirectory();
		main_dir = home + "/" + "Pictures/MegaSync";
		allowed_exts = ("png", "jpg", "jpeg");
		pictures_list = getRandomImages(main_dir, 100, allowed_exts);


	width = 0;
	height = 0;
	#skip portrait pictures
	while (width <= height):
		next_picture = pictures_list.pop();
		with Image.open(next_picture) as im:
			width, height = im.size;
			#print(width, height, next_picture);



	picture_name = os.path.split(next_picture)[1];
	directory = os.path.split(os.path.dirname(next_picture))[1];

	print(next_picture);
	
	setImageWallpaper(next_picture);
	notifiy_on_screen(directory, picture_name);

	savePicturesList(pictures_list);
